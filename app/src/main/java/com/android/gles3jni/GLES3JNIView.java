/*
 * Copyright 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.gles3jni;

import android.content.Context;
import android.graphics.PixelFormat;
import android.opengl.GLSurfaceView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;

import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.content.Context;

import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLDisplay;
import javax.microedition.khronos.opengles.GL10;


class GLES3JNIView extends GLSurfaceView
        implements SensorEventListener {
    private static final String TAG = "GLES3JNI";
    private static final boolean DEBUG = true;
    private Renderer mRenderer;


    public GLES3JNIView(Context context) {
        super(context);

        SensorManager sensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        Sensor sensor = sensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);
        sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_GAME);

        // Pick an EGLConfig with RGB8 color, 16-bit depth, no stencil,
        // supporting OpenGL ES 2.0 or later backwards-compatible versions.
        setEGLConfigChooser(8, 8, 8, 0, 16, 0);
        setEGLContextClientVersion(2);
        mRenderer = new Renderer();
        setRenderer(mRenderer);
    }

    @Override
    public void onSensorChanged(SensorEvent event)
    {
        mRenderer.setOrientation(event.values);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy)
    {

    }


    private static class Renderer implements GLSurfaceView.Renderer {
        private float[] orientation;

        private Renderer() {
            orientation = new float[4];
            orientation[0] = 0.0001f;
            orientation[1] = 0;
            orientation[2] = 0;
            orientation[3] = 1.0f;
        }

        public void onDrawFrame(GL10 gl) {
            GLES3JNILib.step(orientation);
        }

        public void onSurfaceChanged(GL10 gl, int width, int height) {
            GLES3JNILib.resize(width, height);
        }

        public void onSurfaceCreated(GL10 gl, EGLConfig config) {
            GLES3JNILib.init();
        }

        public void setOrientation(float[] orientation) {
            this.orientation = orientation;
        }
    }
}

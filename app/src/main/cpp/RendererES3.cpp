/*
 * Copyright 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "gles3jni.h"
#include <EGL/egl.h>
#include <string.h>

#define STR(s) #s
#define STRV(s) STR(s)

#define POS_ATTRIB 0
#define COLOR_ATTRIB 1
#define SCALEROT_ATTRIB 2
#define OFFSET_ATTRIB 3

static const char VERTEX_SHADER[] =
    "#version 300 es\n"
    "layout(location = " STRV(POS_ATTRIB) ") in vec2 pos;\n"
    "layout(location=" STRV(COLOR_ATTRIB) ") in vec4 color;\n"
    "layout(location=" STRV(SCALEROT_ATTRIB) ") in vec4 scaleRot;\n"
    "layout(location=" STRV(OFFSET_ATTRIB) ") in vec2 offset;\n"
    "uniform mat4 mvProj;\n"
    "uniform mat4 mView;\n"
    "out vec4 vColor;\n"
    "void main() {\n"
    "    mat2 sr = mat2(scaleRot.xy, scaleRot.zw);\n"
    "    vec4 rotated = vec4(sr*pos + offset, 0.0, 1.0);\n"
    "    gl_Position = mvProj * rotated;\n"
    "    vColor = color;\n"
    "}\n";

static const char FRAGMENT_SHADER[] =
    "#version 300 es\n"
    "precision mediump float;\n"
    "in vec4 vColor;\n"
    "out vec4 outColor;\n"
    "void main() {\n"
    "    outColor = vColor;\n"
    "}\n";

class RendererES3: public Renderer {
public:
    RendererES3();
    virtual ~RendererES3();
    bool init();

private:
    enum {VB_INSTANCE, VB_SCALEROT, VB_OFFSET, VB_MVPROJ, VB_COUNT};

    virtual float* mapOffsetBuf();
    virtual void unmapOffsetBuf();
    virtual float* mapTransformBuf();
    virtual void unmapTransformBuf();
    virtual void draw(unsigned int numInstances, float orientation[4]);

    const EGLContext mEglContext;
    GLuint mProgram;
    GLuint mVB[VB_COUNT];
    GLuint mVBState;
};

Renderer* createES3Renderer() {
    RendererES3* renderer = new RendererES3;
    if (!renderer->init()) {
        delete renderer;
        return NULL;
    }
    return renderer;
}

RendererES3::RendererES3()
:   mEglContext(eglGetCurrentContext()),
    mProgram(0),
    mVBState(0)
{
    for (int i = 0; i < VB_COUNT; i++)
        mVB[i] = 0;
}

bool RendererES3::init() {
    mProgram = createProgram(VERTEX_SHADER, FRAGMENT_SHADER);
    if (!mProgram)
        return false;

    glGenBuffers(VB_COUNT, mVB);
    glBindBuffer(GL_ARRAY_BUFFER, mVB[VB_INSTANCE]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(QUAD), &QUAD[0], GL_STATIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, mVB[VB_SCALEROT]);
    glBufferData(GL_ARRAY_BUFFER, MAX_INSTANCES * 4*sizeof(float), NULL, GL_DYNAMIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, mVB[VB_OFFSET]);
    glBufferData(GL_ARRAY_BUFFER, MAX_INSTANCES * 2*sizeof(float), NULL, GL_STATIC_DRAW);

    glGenVertexArrays(1, &mVBState);
    glBindVertexArray(mVBState);

    glBindBuffer(GL_ARRAY_BUFFER, mVB[VB_INSTANCE]);
    glVertexAttribPointer(POS_ATTRIB, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const GLvoid*)offsetof(Vertex, pos));
    glVertexAttribPointer(COLOR_ATTRIB, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(Vertex), (const GLvoid*)offsetof(Vertex, rgba));
    glEnableVertexAttribArray(POS_ATTRIB);
    glEnableVertexAttribArray(COLOR_ATTRIB);

    glBindBuffer(GL_ARRAY_BUFFER, mVB[VB_SCALEROT]);
    glVertexAttribPointer(SCALEROT_ATTRIB, 4, GL_FLOAT, GL_FALSE, 4*sizeof(float), 0);
    glEnableVertexAttribArray(SCALEROT_ATTRIB);
    glVertexAttribDivisor(SCALEROT_ATTRIB, 1);

    glBindBuffer(GL_ARRAY_BUFFER, mVB[VB_OFFSET]);
    glVertexAttribPointer(OFFSET_ATTRIB, 2, GL_FLOAT, GL_FALSE, 2*sizeof(float), 0);
    glEnableVertexAttribArray(OFFSET_ATTRIB);
    glVertexAttribDivisor(OFFSET_ATTRIB, 1);

    ALOGV("Using OpenGL ES 3.0 renderer");
    return true;
}

RendererES3::~RendererES3() {
    /* The destructor may be called after the context has already been
     * destroyed, in which case our objects have already been destroyed.
     *
     * If the context exists, it must be current. This only happens when we're
     * cleaning up after a failed init().
     */
    if (eglGetCurrentContext() != mEglContext)
        return;
    glDeleteVertexArrays(1, &mVBState);
    glDeleteBuffers(VB_COUNT, mVB);
    glDeleteProgram(mProgram);
}

float* RendererES3::mapOffsetBuf() {
    glBindBuffer(GL_ARRAY_BUFFER, mVB[VB_OFFSET]);
    return (float*)glMapBufferRange(GL_ARRAY_BUFFER,
            0, MAX_INSTANCES * 2*sizeof(float),
            GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_BUFFER_BIT);
}

void RendererES3::unmapOffsetBuf() {
    glUnmapBuffer(GL_ARRAY_BUFFER);
}

float* RendererES3::mapTransformBuf() {
    glBindBuffer(GL_ARRAY_BUFFER, mVB[VB_SCALEROT]);
    return (float*)glMapBufferRange(GL_ARRAY_BUFFER,
            0, MAX_INSTANCES * 4*sizeof(float),
            GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_BUFFER_BIT);
}

void RendererES3::unmapTransformBuf() {
    glUnmapBuffer(GL_ARRAY_BUFFER);
}

struct Matrix4
{
    float val[4][4];

    Matrix4 operator*(const Matrix4 &m2) const
    {
        Matrix4 mult;
        for (int r = 0; r < 4; r++)
        {
            for (int c = 0; c < 4; c++)
            {
                float v = 0;
                for (int i = 0; i < 4; i++)
                    v += val[r][i]*m2.val[i][c];
                mult.val[r][c] = v;
            }
        }
        return mult;
    }

    Matrix4 Transposed() const
    {
        Matrix4 t;
        for (int r = 0; r < 4; r++)
        {
            for (int c = 0; c < 4; c++)
            {
                t.val[r][c] = val[c][r];
            }
        }
        return t;
    }

    static Matrix4 Identity()
    {
        Matrix4 m;
        memset(m.val, 0, sizeof(Matrix4));
        for (int i = 0; i < 4; i++)
            m.val[i][i]= 1.0f;
        return m;
    }

    static Matrix4 Translate(float x, float y, float z)
    {
        Matrix4 t = Identity();
        t.val[0][3] = x;
        t.val[1][3] = y;
        t.val[2][3] = z;
        return t;
    }

    static Matrix4 Rotate(float x, float y, float z, float angle)
    {
        float w = cosf(angle/2);
        float s = sinf(angle/2);
        x *= s;
        y *= s;
        z *= s;
        Matrix4 lhs, rhs;
        lhs.val[0][0] =  w;  lhs.val[0][1] =  z;  lhs.val[0][2] = -y; lhs.val[0][3] = x;
        lhs.val[1][0] = -z;  lhs.val[1][1] =  w;  lhs.val[1][2] =  x; lhs.val[1][3] = y;
        lhs.val[2][0] =  y;  lhs.val[2][1] = -x;  lhs.val[2][2] =  w; lhs.val[2][3] = z;
        lhs.val[3][0] = -x;  lhs.val[3][1] = -y;  lhs.val[3][2] = -z; lhs.val[3][3] = w;

        rhs.val[0][0] =  w;  rhs.val[0][1] =  z;  rhs.val[0][2] = -y; rhs.val[0][3] = -x;
        rhs.val[1][0] = -z;  rhs.val[1][1] =  w;  rhs.val[1][2] =  x; rhs.val[1][3] = -y;
        rhs.val[2][0] =  y;  rhs.val[2][1] = -x;  rhs.val[2][2] =  w; rhs.val[2][3] = -z;
        rhs.val[3][0] =  x;  rhs.val[3][1] =  y;  rhs.val[3][2] =  z; rhs.val[3][3] = w;

        return lhs*rhs;
    }

    static Matrix4 Projection(float Sx, float Sy, float near, float far)
    {
        Matrix4 p = Identity();
        p.val[0][0] = Sx;
        p.val[1][1] = Sy;
        p.val[2][2] = -(far/(far-near)); p.val[2][3] = -(far/(far-near));
        p.val[3][2] = -1; p.val[3][3] = 0;
        return p;
    }
};

void RendererES3::draw(unsigned int numInstances, float orientation[4]) {
    glUseProgram(mProgram);

    const float far = 100.0f;
    const float near = .1f;

    float angle = acosf(orientation[3])*2;
    float length = sqrtf(orientation[0]*orientation[0] + orientation[1]*orientation[1] + orientation[2]*orientation[2]);
    float x = orientation[0] / length;
    float y = orientation[1] / length;
    float z = orientation[2] / length;
    if (!isfinite(x))
    {
       x = 1, y = 0, z = 0, angle = 0;
    }

    Matrix4 modelViewMat = Matrix4::Translate(0, 0, -1.0f) * Matrix4::Rotate(x, y, z, angle);
    Matrix4 mvProjectionMat = Matrix4::Projection(1.0f, 1.0f, near, far) * modelViewMat;

    GLuint projectionLocation = glGetUniformLocation(mProgram, "mvProj");
    glUniformMatrix4fv(projectionLocation, 1, true, &(mvProjectionMat.val[0][0]));


    GLuint modelViewLocation = glGetUniformLocation(mProgram, "mView");
    glUniformMatrix4fv(modelViewLocation, 1, true, &(modelViewMat.val[0][0]));

    glBindVertexArray(mVBState);
    glDrawArraysInstanced(GL_TRIANGLE_STRIP, 0, 4, numInstances);
}
